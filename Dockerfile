FROM nginx:latest

RUN apt-get update -y && \
    apt-get install nginx -y

COPY static/ /usr/share/nginx/html/

EXPOSE 80

CMD ["nginx","-g","daemon off;"]
